<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

/**
 * Interface to notify admin by sms or email
 * @package AppBundle\Utils
 */
interface NotificationHandlerInterface
{
    /**
     * Notify admin by sms
     * @param $message
     */
    public function notifyBySms(NotificationMessageInterface $message);

    /**
     * Notify admin by email
     * @param $message
     */
    public function notifyByEmail(NotificationMessageInterface $message);

}