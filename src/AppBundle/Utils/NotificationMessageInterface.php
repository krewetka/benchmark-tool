<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

/**
 * Interface of simple notification email
 * @package AppBundle\Utils
 */
interface NotificationMessageInterface
{
    /**
     * NotificationMessageInterface constructor.
     * @param  string $subject
     * @param  string $text
     */
    public function __construct(string $subject, string $text);

    /**
     * @return string
     */
    public function getSubject() : string ;

    /**
     * @return string
     */
    public function getText() : string ;

}