<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

/**
 * Simple notification message to sms or email
 * @package AppBundle\Utils
 */
class NotificationMessage implements NotificationMessageInterface
{
    private $subject;
    private $text;

    /**
     * Message constructor.
     * @param $subject
     * @param $text
     */
    public function __construct(string $subject, string $text)
    {
        $this->subject = $subject;
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getSubject() : string
    {
        return $this->subject;
    }

    /**
     *  @return string
     */
    public function getText() : string
    {
        return $this->text;
    }




}