<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

use AppBundle\Notifiers\NotifierInterface;
use Exception;
use Monolog\Logger;

/**
 * Notifies admin by email or sms
 * @package AppBundle\Utils
 */
class NotificationHandler implements NotificationHandlerInterface
{

    /**
     * @var NotifierInterface email notification service
     */
    private $emailNotifier;
    /**
     * @var NotifierInterface sms notification service
     */
    private $smsNotifier;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * NotificationHandler constructor.
     * @param NotifierInterface $emailNotifier email notification service
     * @param NotifierInterface $smsNotifier sms notification service
     * @param Logger $logger
     */
    public function __construct(NotifierInterface $emailNotifier, NotifierInterface $smsNotifier, Logger $logger)
    {
        $this->emailNotifier = $emailNotifier;
        $this->smsNotifier = $smsNotifier;
        $this->logger = $logger;
    }


    /**
     * @inheritdoc
     * @param $message
     */
    public function notifyByEmail(NotificationMessageInterface $message)
    {
        try {
            $this->emailNotifier->notifyAdmin($message);
            $this->logger->info('Email sent');
        }catch(Exception $ex){
            $this->logger->info('Problem sending email: '.$ex->getMessage());
        }

    }


    /**
     * @inheritdoc
     * @param $message
     */
    public function notifyBySms(NotificationMessageInterface $message)
    {
        try {
            $this->smsNotifier->notifyAdmin($message);
            $this->logger->info('SMS sent');

        }catch(Exception $ex){
            $this->logger->info('Problem sending SMS: '.$ex->getMessage());
        }
    }
}