<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

use AppBundle\Model\WebsiteInterface;

/**
 * Interface to measure website loading time
 * @package AppBundle\Utils
 */
interface HttpBenchmarkInterface
{
    /**
     * Measure http loading time of the website
     * @param WebsiteInterface $website
     * @return int loading time in miliseconds
     *
     */

    public function getWebsiteLoadingTime(WebsiteInterface $website);
}