<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

use AppBundle\Model\WebsiteInterface;
use Circle\RestClientBundle\Services\RestInterface;
use Symfony\Component\Stopwatch\Stopwatch;

/**
 * Measure benchmark time. It uses restLibrary and stopwatch.
 * @package AppBundle\Utils
 */
class HttpBenchmarkHandler implements HttpBenchmarkInterface
{
    /**
     * Timeout limit for checking single website
     * @var int
     */
    private $timeoutLimit = 30;

    /**
     * @var RestInterface api to measure loading time
     */
    private $restLibrary;

    /**
     * HttpBenchmarkHandler constructor.
     * @param RestInterface $restLibrary
     */
    public function __construct(RestInterface $restLibrary)
    {
        $this->restLibrary = $restLibrary;
    }

    /**
     * @inheritdoc
     * @param WebsiteInterface $website
     * @return int
     */
    public function getWebsiteLoadingTime(WebsiteInterface $website)
    {
        $url = $website->getUrl();

        return $this->getUrlLoadingTime($url);

    }

    /**
     * Measure http loading time of url address
     * @param string $url
     * @return int loading time in miliseconds
     */
    public function getUrlLoadingTime(string $url)
    {

        $options = [CURLOPT_CONNECTTIMEOUT => $this->timeoutLimit];
        $stopwatch = new Stopwatch();

        //it's necessary because otherwise website benchmark time was incorrect
        $this->restLibrary->connect($url, $options);

        $stopwatch->start('benchmarkStopwatch');
        $this->restLibrary->get($url, $options);
        $event = $stopwatch->stop('benchmarkStopwatch');

        return $event->getDuration();

    }

}