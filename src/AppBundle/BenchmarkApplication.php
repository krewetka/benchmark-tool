<?php
declare(strict_types = 1);
namespace AppBundle;

use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Component\Console\Input\InputDefinition;
use Symfony\Component\Console\Input\InputInterface;

/**
 * Main class for application
 * @package AppBundle
 */
class BenchmarkApplication extends Application
{

    /**
     * Gets the name of the command based on input. In our case we have only one command.
     *
     * @param InputInterface $input The input interface
     * @return string The command name
     */
    protected function getCommandName(InputInterface $input)
    {
        return 'benchmark';
    }


    /**
     * @inheritdoc
     *
     * @return InputDefinition
     */
    public function getDefinition()
    {
        $inputDefinition = parent::getDefinition();
        $inputDefinition->setArguments();

        return $inputDefinition;
    }

}