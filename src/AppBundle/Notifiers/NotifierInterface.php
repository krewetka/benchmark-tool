<?php
declare(strict_types = 1);

namespace AppBundle\Notifiers;

use AppBundle\Utils\NotificationMessageInterface;


/**
 * Interface for admin notification
 * @package AppBundle\Utils
 */
interface NotifierInterface
{
    /**
     * Notify admin about message
     * @param NotificationMessageInterface $message message to send to admin
     */
    public function notifyAdmin(NotificationMessageInterface $message);
}