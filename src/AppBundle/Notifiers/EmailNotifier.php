<?php
declare(strict_types = 1);

namespace AppBundle\Notifiers;

use AppBundle\Utils\NotificationMessageInterface;
use Swift_Mailer;

/**
 * Email notification service
 * @package AppBundle\Utils
 */
class EmailNotifier implements NotifierInterface
{
    /**
     * @var string admin's email address
     */
    private $adminEmail;
    /**
     * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var string sender's email
     */
    private $senderEmail;

    /**
     * EmailNotifier constructor.
     * @param Swift_Mailer $mailer
     * @param string $adminEmail admin's email address
     * @param string $senderEmail sender's email
     */
    public function __construct(Swift_Mailer $mailer, string $adminEmail, string $senderEmail)
    {
        $this->adminEmail = $adminEmail;
        $this->mailer = $mailer;
        $this->senderEmail = $senderEmail;
    }

    /**
     * Sends email to admin
     * @param NotificationMessageInterface $message message to send to admin
     */
    public function notifyAdmin(NotificationMessageInterface $message)
    {

        $subject = $message->getSubject();
        $text = $message->getText();

        $message = \Swift_Message::newInstance()
            ->setSubject($subject)
            ->setFrom($this->senderEmail)
            ->setTo($this->adminEmail)
            ->setBody($text , 'text/plain');

        $this->mailer->send($message);

    }
}