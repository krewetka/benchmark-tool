<?php
declare(strict_types = 1);

namespace AppBundle\Notifiers;

use AppBundle\Utils\NotificationMessageInterface;
use SMSApi\Api\SmsFactory;
use SMSApi\Client;


/**
 * SMS Notification service using SMSApi package
 * @package AppBundle\Utils
 */
class SmsNotifier implements NotifierInterface
{
    /**
     * @var string admin's phone
     */
    private $adminPhone;

    /**
     * @var string token necessary to smsapi.pl service
     */
    private $apiToken;

    /**
     * @var string sender's "name" in SMS
     */
    private $smsSenderName;

    /**
     * EmailNotifier constructor.
     * @param string $adminPhone admin's phone
     * @param string $apiToken token necessary to smsapi.pl service
     * @param string $smsSenderName  string sender's "name" in SMS
     */
    public function __construct(string $adminPhone, string $apiToken, string $smsSenderName)
    {
        $this->adminPhone = $adminPhone;
        $this->apiToken = $apiToken;
        $this->smsSenderName = $smsSenderName;
    }

    /**
     * Sends SMS to admin
     * @param NotificationMessageInterface $message message to send to admin
     */
    public function notifyAdmin(NotificationMessageInterface $message)
    {

        $client = Client::createFromToken($this->apiToken);

        $smsApi = new SmsFactory;
        $smsApi->setClient($client);

        $subject = $message->getSubject();
        $text = $message->getText();

        $actionSend = $smsApi->actionSend();

        $actionSend->setTo($this->adminPhone);
        $actionSend->setText($subject.". ".$text);
        $actionSend->setSender($this->smsSenderName);

        $actionSend->execute();

    }
}