<?php
declare(strict_types = 1);

namespace AppBundle\Command;

use AppBundle\Model\BenchmarkHandler;
use AppBundle\View\BenchmarkViewInterface;
use DateTime;
use Exception;
use InvalidArgumentException;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Takes input params pass it to the model and return after benchmark
 * @package AppBundle\Command
 */
class BenchmarkCommand extends Command
{
    /**
     * @var BenchmarkHandler main application login
     */
    protected $model;
    /**
     * @var BenchmarkViewInterface view to create table results
     */
    protected $view;
    /**
     * @var InputInterface console input
     */
    protected $input;
    /**
     * @var OutputInterface console output
     */
    protected $output;

    /**
     * BenchmarkCommand constructor.
     * @param BenchmarkHandler $model main application login
     * @param BenchmarkViewInterface $view to create table results
     */
    public function __construct(BenchmarkHandler $model, BenchmarkViewInterface $view)
    {
        $this->model = $model;
        $this->view = $view;

        parent::__construct();
    }

    /**
     * Configures parameters for the command
     */
    protected function configure()
    {
        $this
            ->setName('benchmark')
            // the short description shown while running "php bin/console list"
            ->setDescription('Run benchmark test')
            ->setHelp("This command allows you to benchmark website against it's competitors")
            ->addArgument(
                'website',
                InputArgument::REQUIRED,
                'Url of website to run benchmark test for'
            )
            ->addArgument(
                'competitors',
                InputArgument::IS_ARRAY | InputArgument::REQUIRED,
                'Urls of competitors website (separate multiple names with a space)'
            );
    }

    /**
     *  Main execute action of this app. Gets input params, pass it to model and present final result to console.
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->input = $input;
        $this->output = $output;

        $this->displayEnteredParams();

        $websiteUrl = $input->getArgument('website');
        $competitorsUrls = $input->getArgument('competitors');

        try {

            $result = $this->model->performBenchmark($websiteUrl, $competitorsUrls);

            $tableView = $this->view->generateTableView($result);

            $this->renderResult($tableView);

        } catch (InvalidArgumentException $ex) {
            $this->writeLine($ex->getMessage());
            $this->writeLine("CHANGE PARAMS AND TRY AGAIN.");

        } catch (Exception $ex) {
            $this->writeLine($ex->getMessage());
            $this->writeLine("PROBLEM EXECUTING COMMAND");
        }

        return 0;

    }

    /**
     * Shows entered parameters in console
     */
    private function displayEnteredParams()
    {
        $input = $this->input;

        $ourWebsite = $input->getArgument('website');
        $competitors = $input->getArgument('competitors');

        $date = new DateTime();
        $formattedDate = $date->format('d.m.Y');

        $this->writeLine('BENCHMARK TEST '.$formattedDate);

        $this->writeLine('BENCHMARK PARAMETERS:');

        $this->writeLine('OUR Website: '.$ourWebsite);

        $this->writeLine('OUR Competitors: '.implode(", ", $competitors));

    }

    /**
     * Writes single line with extra space
     * @param string $text line of text
     */
    private function writeLine(string $text)
    {

        $output = $this->output;
        $output->writeln('');
        $output->writeln($text);

    }

    /**
     * Renders table result to console
     * @param array $result result of benchmark action
     */
    private function renderResult(array $result)
    {

        $output = $this->output;
        $this->writeLine('BENCHMARK RESULT:');

        $table = new Table($output);

        $headers = $result['headers'];
        $rows = $result['rows'];

        $table->setHeaders($headers);
        $table->setRows($rows);
        $table->render();

    }
}