<?php
declare(strict_types = 1);

namespace AppBundle\View;

use AppBundle\Model\WebsiteInterface;


/**
 * Interface for website table view creation
 * @package AppBundle\View
 */
interface BenchmarkViewInterface
{
    /**
     * Generate table view
     * @param WebsiteInterface[] $websites
     * @return array structure with header and rows
     */
    public function generateTableView(array $websites): array;

}