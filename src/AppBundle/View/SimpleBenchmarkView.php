<?php
declare(strict_types = 1);

namespace AppBundle\View;

use AppBundle\Model\WebsiteInterface;

/**
 *  Simple unsorted result table
 * @package AppBundle\View
 */
class SimpleBenchmarkView extends AbstractBenchmarkView
{
    /**
     * @inheritdoc
     * @return array
     */
    protected function createTableHeader()
    {
        $header = ['Time[ms]','Competitor'];

        return $header;
    }

    /**
     * @inheritdoc
     * @param WebsiteInterface $website
     * @return array single rows to present
     */
    protected function createTableRow(WebsiteInterface $website)
    {
        $row = [];

        $row['url'] = $website->getUrl();
        $row['time'] = $website->getBenchmarkTime();
        $row['competitor'] = $website->isCompetitor() ? 1 : 0;

        return $row;
    }


}