<?php
declare(strict_types = 1);

namespace AppBundle\View;

use AppBundle\Model\WebsiteInterface;

/**
 * Generate view of results to display
 * @package AppBundle\View
 */
abstract class AbstractBenchmarkView implements BenchmarkViewInterface
{
    /**
     * @inheritdoc
     * @param WebsiteInterface[] $websites
     * @return array structure with header and rows
     */
    public function generateTableView(Array $websites): array
    {

        $result['headers'] = $this->createTableHeader();

        $rows = [];

        /** @var WebsiteInterface $website */
        foreach ($websites as $website) {
            $rows[] = $this->createTableRow($website);
        }

        $result['rows'] = $rows;

        return $result;

    }

    /**
     * Creates header row for result table
     * @return array
     */
    protected abstract function createTableHeader();

    /**
     * Creates header row for result table
     * @param WebsiteInterface $website
     * @return array
     */
    protected abstract function createTableRow(WebsiteInterface $website);
}