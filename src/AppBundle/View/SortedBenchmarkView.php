<?php
declare(strict_types = 1);

namespace AppBundle\View;

use AppBundle\Model\WebsiteInterface;

/**
 * Sorted view of all websites
 * @package AppBundle\View
 */
class SortedBenchmarkView extends AbstractBenchmarkView
{
    /**
     * Returns table result. In this case it is sorted by benchmark time
     * @param WebsiteInterface[] $websites
     * @return array structure with header and rows
     */
    public function generateTableView(Array $websites): array
    {

        usort(
            $websites,
            function (WebsiteInterface $website1, WebsiteInterface $website2) {
                return $website1->getBenchmarkTime() <=> $website2->getBenchmarkTime();
            }
        );

        return parent::generateTableView($websites);
    }

    /**
     * @inheritdoc
     * @return array
     */
    protected function createTableHeader()
    {
        $header = ['Url', 'Time[ms]', 'Website Type', 'Comment'];

        return $header;
    }

    /**
     * @inheritdoc
     * @param WebsiteInterface $website
     * @return array single rows to present
     */
    protected function createTableRow(WebsiteInterface $website)
    {
        $row = [];
        $row['url'] = $website->getUrl();
        $row['time'] = $website->getBenchmarkTime();
        $row['type'] = $website->getWebsiteType();
        $row['desc'] = $website->getComment();

        return $row;
    }


}