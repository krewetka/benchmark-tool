<?php
declare(strict_types = 1);

namespace AppBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Main class for AppBundle
 * @package AppBundle
 */
class AppBundle extends Bundle
{
}
