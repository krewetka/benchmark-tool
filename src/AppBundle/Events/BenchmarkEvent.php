<?php
declare(strict_types = 1);
namespace AppBundle\Events;

use AppBundle\Model\WebsiteInterface;
use Symfony\Component\EventDispatcher\Event as BaseEvent;

/**
 * Event passed to SlowWebsiteListener
 * @package AppBundle\Events
 */
class BenchmarkEvent extends BaseEvent
{
    /**
     * @var WebsiteInterface our website
     */
    private $website;
    /**
     * @var WebsiteInterface[] our competitors
     */
    private $competitors;

    /**
     * BenchmarkEvent constructor.
     * @param WebsiteInterface $website our website
     * @param WebsiteInterface[] $competitors our competitors
     */
    public function __construct(WebsiteInterface $website, array $competitors)
    {
        $this->website = $website;
        $this->competitors = $competitors;
    }

    /**
     * Returns our website
     * @return WebsiteInterface
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Returns array of competitors
     * @return array
     */
    public function getCompetitors()
    {
        return $this->competitors;
    }


}