<?php
declare(strict_types = 1);

namespace AppBundle\Model;

use AppBundle\Events\BenchmarkEvent;
use AppBundle\Utils\HttpBenchmarkInterface;
use Exception;
use Monolog\Logger;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

/**
 * Performs main logic of benchmark process
 * @package AppBundle\Model
 */
class BenchmarkHandler implements PerformBenchmarkInterface
{
    /**
     * @var TraceableEventDispatcher dispatcher to dispatch events about slow website
     */
    protected $eventDispatcher;
    /**
     * @var Logger
     */
    protected $logger;
    /**
     * @var HttpBenchmarkInterface handler to measure http request time
     */
    private $httpTimeMeasurer;
    /**
     * @var WebsiteFactory factory to create websites
     */
    private $websiteFactory;

    /**
     * BenchmarkHandler constructor.
     * @param HttpBenchmarkInterface $urlTimeMeasurer handler to measure http request time
     * @param TraceableEventDispatcher $eventDispatcher dispatcher to dispatch events about slow website
     * @param Logger $logger
     * @param WebsiteFactory $WebsiteFactory factory to create websites
     */
    public function __construct(
        HttpBenchmarkInterface $urlTimeMeasurer,
        TraceableEventDispatcher $eventDispatcher,
        Logger $logger,
        WebsiteFactory $WebsiteFactory
    ) {

        $this->httpTimeMeasurer = $urlTimeMeasurer;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
        $this->websiteFactory = $WebsiteFactory;
    }

    /**
     * Main application logic. Performs main logic of checking loading times and comparing it.
     * @param string $websiteUrl our website url
     * @param array $competitorsUrls array of competitors we want to check against
     * @return array array with all websites
     */
    public function performBenchmark(string $websiteUrl, array $competitorsUrls): array
    {
        $ourWebsite = $this->createWebsite($websiteUrl, 'our_website');

        $competitors = [];
        foreach ($competitorsUrls as $url) {
            $competitors[] = $this->createWebsite($url, 'competitor');
        }

        $allWebsites = array_merge([$ourWebsite], $competitors);

        /** @var WebsiteInterface $website */
        foreach ($allWebsites as $website) {
            $this->getWebsiteLoadingTime($website);
        }

        $this->compareWebsiteToCompetitors($ourWebsite, $competitors);

        return $allWebsites;
    }

    /**
     * Compare website and competitors and dispatch events if website is slow
     * @param WebsiteInterface $website our website
     * @param WebsiteInterface[] $competitors our competitors
     */
    public function compareWebsiteToCompetitors(WebsiteInterface $website, array $competitors)
    {

        $isSlower = false;
        $isMuchSlower = false;

        /** @var WebsiteInterface $competitor */
        foreach ($competitors as $competitor) {

            $this->setCompetitorResult($website, $competitor);

            if ($website->isWebsiteSlowerThan($competitor)) {
                $isSlower = true;
            }

            if ($website->isWebsiteMuchSlowerThan($competitor)) {
                $isMuchSlower = true;
            }
        }

        if ($isMuchSlower) {
            $this->dispatchEvent('benchmark.extra.slow', $website, $competitors);
        } elseif ($isSlower) {
            $this->dispatchEvent('benchmark.slow', $website, $competitors);
        }

    }

    /**
     * Sets comparison result as a text comment in competitor website
     * @param WebsiteInterface $website our website
     * @param WebsiteInterface $competitor one of our competitors
     */
    private function setCompetitorResult(WebsiteInterface $website, WebsiteInterface $competitor)
    {

        $percent = $competitor->getPercentageDifferenceFrom($website);

        if($percent != 0) {

            $verb = $percent > 0 ? "FASTER" : "SLOWER";

            $message = sprintf("IS %5.1f%% %s", abs($percent), $verb);

            $competitor->setComment($message);
        }

    }

    /**
     * Creates and dispatch event with details about website and competitors
     * @param string $eventType type of even to dispatch
     * @param WebsiteInterface $website our website
     * @param array $competitors our competitors
     */
    private function dispatchEvent($eventType, $website, $competitors)
    {
        $this->eventDispatcher->dispatch($eventType, new BenchmarkEvent($website, $competitors));
    }

    /**
     * Check loading time for single website
     * @param WebsiteInterface $website
     */
    private function getWebsiteLoadingTime(WebsiteInterface $website)
    {
        try {
            $time = $this->httpTimeMeasurer->getWebsiteLoadingTime($website);
            $website->setBenchmarkTime($time);
            $this->logger->info("Website checked: $website");
        } catch (Exception $ex) {

            $this->logger->warning("Website checking problem: ".$ex->getMessage());
            $website->setComment($ex->getMessage());
        }
    }


    /**
     * Create website of selected type
     * @param string $websiteUrl
     * @param string $websiteType
     * @return WebsiteInterface
     */
    private function createWebsite(string $websiteUrl, string $websiteType)
    {
        $factory = $this->websiteFactory;

        return $factory::build($websiteUrl, $websiteType);

    }


}