<?php
declare(strict_types = 1);

namespace AppBundle\Model;

/**
 * Competitor of our website
 * @package AppBundle\Model
 */
class Competitor extends AbstractWebsite
{

    /**
     * Competitor constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->isCompetitor = true;
        parent::__construct($url);
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getWebsiteType(): string
    {
        return 'COMPETITOR';
    }
}