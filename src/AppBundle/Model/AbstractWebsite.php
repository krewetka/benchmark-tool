<?php
declare(strict_types = 1);

namespace AppBundle\Model;

use InvalidArgumentException;
use Symfony\Component\Validator\Constraints\Url;
use Symfony\Component\Validator\Validation;

/**
 * Abstract website class which holds website url and benchmark time.
 * Probably should be divided in more than one interface.
 * @package AppBundle\Model
 */
abstract class AbstractWebsite implements WebsiteInterface
{

    /**
     * @var bool if website is our competitor
     */
    protected $isCompetitor;
    /**
     * @var string url to check loading time
     */
    private $url;
    /**
     * @var integer checked loading time
     */
    private $benchmarkTime;
    /**
     * @var string additional comment to display
     */
    private $comment;

    /**
     * AbstractWebsite constructor.
     * @param string $url url to check loading time
     */
    public function __construct(string $url)
    {

        $basicUrlCorrect = $this->checkIfCorrectUrl($url);

        if($basicUrlCorrect) {
            $this->url = $url;
        }else{

            $urlWithHttp = $this->addHttpIfMissing($url);
            $withHttpCorrect = $this->checkIfCorrectUrl($urlWithHttp);

            if($withHttpCorrect){
                //adding http helped so we use this version
                $this->url = $urlWithHttp;
            }else{
                throw new InvalidArgumentException('Incorrect URL: '.$url);
            }
        }

    }

    /**
     * Checks is given url is correct or not
     * @param string $url
     * @return bool
     */
    private function checkIfCorrectUrl(string $url)
    {

        $validator = Validation::createValidator();

        $urlConstraint = new Url();
        //$urlConstraint->checkDNS = true;

        $violations = $validator->validate($url, [$urlConstraint]);

        if (0 !== count($violations)) {
            return false;
        }

        return true;
    }

    /**
     * Add http:// prefix to the url it it does not contain http:// or https://
     * @param string $url
     * @return string returns corrected url
     */
    private function addHttpIfMissing(string $url){

        $parsed = parse_url($url);
        if (empty($parsed['scheme'])) {
            $url = 'http://' . ltrim($url, '/');
        }

        return $url;
    }

    /**
     * @inheritdoc
     * @return bool
     */
    public function isCompetitor(): bool
    {
        return $this->isCompetitor;
    }

    /**
     * @inheritdoc
     * @param WebsiteInterface $other website to compare against
     * @return bool
     */
    public function isWebsiteSlowerThan(WebsiteInterface $other): bool
    {

        if ($this->getBenchmarkTime() === 0 || $other->getBenchmarkTime() === 0) {
            return false;
        }

        return $this->getBenchmarkTime() > $other->getBenchmarkTime();
    }

    /**
     * @inheritdoc
     * @param WebsiteInterface $other website to compare against
     * @return bool
     */
    public function isWebsiteMuchSlowerThan(WebsiteInterface $other): bool
    {

        if ($this->getBenchmarkTime() === 0 || $other->getBenchmarkTime() === 0) {
            return false;
        }

        return $this->getBenchmarkTime() / 2 > $other->getBenchmarkTime();
    }

    /**
     * @inheritdoc
     * @return int
     */
    public function getBenchmarkTime() : int
    {
        return $this->benchmarkTime ?? 0;
    }

    /**
     * @inheritdoc
     * @param int $time
     */
    public function setBenchmarkTime(int $time)
    {
        $this->benchmarkTime = $time;
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function __toString(): string
    {
        return $this->getWebsiteType()." | ".$this->getUrl()." | ".$this->getBenchmarkTime()."ms";
    }

    /**
     * @inheritdoc
     * @return string
     */
    public abstract function getWebsiteType(): string;

    /**
     * @inheritdoc
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getComment(): string
    {
        return $this->comment ?? '';
    }

    /**
     * @inheritdoc
     * @param string $comment
     */
    public function setComment(string $comment)
    {
        $this->comment = $comment;
    }

    /**
     * @inheritdoc
     * @param WebsiteInterface $other
     * @return int result percentage number (0-100)
     */
    public function getPercentageDifferenceFrom(WebsiteInterface $other)
    {

        if ($this->getBenchmarkTime() === 0 || $other->getBenchmarkTime() === 0) {
            return 0;
        }

        $myTime = $this->getBenchmarkTime();
        $otherTime = $other->getBenchmarkTime();

        return (1 - $myTime / $otherTime) * 100;
    }


}