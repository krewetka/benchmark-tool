<?php
declare(strict_types = 1);

namespace AppBundle\Model;

/**
 * Creates different types of websites
 * @package AppBundle\Model
 */
class WebsiteFactory
{

    /**
     * For given url, creates website of selected type
     * @param string $url
     * @param string $type type of website to create
     * @return WebsiteInterface
     */
    public static function build(string $url, string $type): WebsiteInterface
    {

        switch ($type) {
            case 'our_website':
                return new OurWebsite($url);
            case 'competitor':
                return new Competitor($url);
            default:
                throw new \InvalidArgumentException("$type is not a valid website type");
        }
    }
}