<?php
declare(strict_types = 1);

namespace AppBundle\Model;

/**
 * Our website model
 * @package AppBundle\Model
 */
class OurWebsite extends AbstractWebsite
{

    /**
     * OurWebsite constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->isCompetitor = false;
        parent::__construct($url);
    }

    /**
     * @inheritdoc
     * @return string
     */
    public function getWebsiteType(): string
    {
        return 'OUR WEBSITE';
    }
}