<?php
declare(strict_types = 1);

namespace AppBundle\Model;

/**
 * Website Interface which keeps main actions of all types of websites
 * @package AppBundle\Model
 */
interface WebsiteInterface
{
    /**
     * WebsiteInterface constructor.
     * @param string $url
     */
    public function __construct(string $url);

    /**
     * Sets benchmark time
     * @param int $time
     */
    public function setBenchmarkTime(int $time);

    /**
     * Returns benchmark time
     * @return int
     */
    public function getBenchmarkTime(): int;

    /**
     * Returns url of website
     * @return string
     */
    public function getUrl(): string;

    /**
     * Returns website type
     * @return string
     */
    public function getWebsiteType(): string;

    /**
     * Returns if website is our competitor not
     * @return bool
     */
    public function isCompetitor(): bool;
    
    /**
     * Checks if our website is slower than other website
     * @param WebsiteInterface $other
     * @return bool
     */
    public function isWebsiteSlowerThan(WebsiteInterface $other): bool;

    /**
     * Checks if our website is twice slower than other website
     * @param WebsiteInterface $other
     * @return bool
     */
    public function isWebsiteMuchSlowerThan(WebsiteInterface $other): bool;

    /**
     * @return string
     */
    public function __toString(): string;

    /**
     * Sets additional comment
     * @param string $comment
     */
    public function setComment(string $comment);

    /**
     * Returns additional comment
     * @return string
     */
    public function getComment(): string;

    /**
     * Returns how much slower/faster our competitor is
     * @param WebsiteInterface $other
     * @return integer result percentage number (0-100)
     */
    public function getPercentageDifferenceFrom(WebsiteInterface $other);


}