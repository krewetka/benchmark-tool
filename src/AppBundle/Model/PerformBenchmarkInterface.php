<?php
declare(strict_types = 1);

namespace AppBundle\Model;

/**
 * Interface which allows to replace main benchmark logic
 * @package AppBundle\Model
 */
interface PerformBenchmarkInterface
{
    /**
     * Performs main logic of checking loading times and comparing it.
     * @param string $website our website
     * @param array $competitors array of our competitors
     * @return array array of all websites
     */
    public function performBenchmark(string $website, array $competitors): array;

}