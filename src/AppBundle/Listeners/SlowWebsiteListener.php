<?php
declare(strict_types = 1);

namespace AppBundle\Listeners;

use AppBundle\Events\BenchmarkEvent;
use AppBundle\Utils\NotificationHandlerInterface;
use AppBundle\Utils\NotificationMessage;
use Monolog\Logger;

/**
 * Listens for slow websites events dispatched
 * @package AppBundle\Listeners
 */
class SlowWebsiteListener
{
    /**
     * @var NotificationHandlerInterface handler to notify admin in case of problem
     */
    private $notificationHandler;
    /**
     * @var Logger
     */
    private $logger;

    /**
     * SlowWebsiteListener constructor.
     * @param NotificationHandlerInterface $notificationHandler handler to notify admin in case of problem
     * @param Logger $logger
     */
    public function __construct(NotificationHandlerInterface $notificationHandler, Logger $logger)
    {
        $this->notificationHandler = $notificationHandler;
        $this->logger = $logger;
    }

    /**
     * Notify admin when our website is slow in comparison to competitors
     * @param BenchmarkEvent $event event passed to notification handler
     */
    public function onBenchmarkSlow(BenchmarkEvent $event)
    {

        $messageText = 'Our website is slow';
        $this->logger->warning($messageText);
        $message = $this->prepareMessage($event, $messageText);
        $this->notificationHandler->notifyByEmail($message);
    }

    /** Notify admin when our website is really slow in comparison to competitors
     * @param BenchmarkEvent $event event passed to notification handler
     */
    public function onBenchmarkExtraSlow(BenchmarkEvent $event)
    {

        $messageText = 'Our website is extremely slow';
        $this->logger->emergency($messageText);
        $message = $this->prepareMessage($event, $messageText);
        $this->notificationHandler->notifyByEmail($message);
        $this->notificationHandler->notifyBySms($message);

    }

    /**
     * Creates message which will be send to admin
     * @param BenchmarkEvent $event event with details passed from listener
     * @param string $subject
     * @return NotificationMessage
     */
    private function prepareMessage(BenchmarkEvent $event, string $subject): NotificationMessage
    {

        $website = $event->getWebsite();
        $messageText = sprintf("Website: %s. Time: %dms.", $website->getUrl(), $website->getBenchmarkTime());
        $message = new NotificationMessage($subject, $messageText);

        return $message;
    }


}