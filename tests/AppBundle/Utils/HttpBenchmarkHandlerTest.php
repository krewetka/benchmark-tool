<?php
declare(strict_types = 1);

namespace AppBundle\Utils;

use AppBundle\Model\WebsiteFactory;

class HttpBenchmarkHandlerTest extends \PHPUnit_Framework_TestCase
{

    /** Provide correct urls and benchmark times
     * @return array
     */
    public function websiteTimesDataProvider()
    {
        return [
            ['http://wp.pl',4],
            ['http://onet.pl',5],
            ['http://gazeta.pl',1]
        ];
    }

    /** Tests loading time with api mock
     * @dataProvider websiteTimesDataProvider
     * @param string $url
     * @param string $time
     */

    public function testGetWebsiteLoadingTime(string $url, string $time)
    {

        $factory = new WebsiteFactory();
        $website = $factory::build($url,'competitor');

        $httpHandler =  $this->getMockBuilder(HttpBenchmarkHandler::class)
            ->setMethods(['getUrlLoadingTime'])
            ->disableOriginalConstructor()
            ->getMock();
        $httpHandler->method('getUrlLoadingTime')->willReturn($time);

        $measuredTime = $httpHandler->getWebsiteLoadingTime($website);
        self::assertEquals($time, $measuredTime);

    }

}
