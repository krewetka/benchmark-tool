<?php
declare(strict_types = 1);

namespace AppBundle\Model;

use InvalidArgumentException;


/**
 * Class OurWebsiteTest
 * @package AppBundle\Model
 */
class OurWebsiteTest extends \PHPUnit_Framework_TestCase
{
    private $correctUrl = 'http://wp.pl';
    private $incorrectUrl = 'kttp://aaa';

    /**
     *  Tests creation of our website with incorrect url
     * @expectedException     InvalidArgumentException
     **/
    public function testConstructorWithIncorrectParam(){

        $website = new OurWebsite($this->incorrectUrl);
    }

    /**
     *  Tests creation of our website with correct url
     */
    public function testConstructorWithCorrectParam(){

        $website = new OurWebsite($this->correctUrl);

        static::assertInstanceOf('AppBundle\Model\OurWebsite', $website);
        static::assertEquals('OUR WEBSITE', $website->getWebsiteType());
    }
}
