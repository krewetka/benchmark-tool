<?php
declare(strict_types = 1);

namespace AppBundle\Model;

use InvalidArgumentException;

/**
 * Class WebsiteFactoryTest
 * @package AppBundle\Model
 */
class WebsiteFactoryTest extends \PHPUnit_Framework_TestCase
{

    /**
     * @var WebsiteFactory
     */
    private static $factory;
    private $correct_url = 'http://wp.pl';

    /**
     *  Examples of incorrect website types and urls
     * @return array
     */
    public function incorrectDataProvider()
    {
        return [
            ["htt://www",'our_website'],
            ["htt://www",'competitor'],
            ["www..",'our_website'],
            ["www..",'competitor'],
        ];
    }

    /**
     * Examples of correct website types and urls
     * @return array
     */
    public function correctDataProvider()
    {
        return [
            ["http://wp.pl",'our_website'],
            ["http://wp.pl",'competitor'],
            ["http://onet.pl",'our_website'],
            ["http://onet.pl",'competitor'],
        ];
    }

    public static function setUpBeforeClass()
    {
        self::$factory = new WebsiteFactory();
    }

    /**
     * Test creation of our website
     */
    public function testFactoryCreationOurWebsite()
    {
        $factory = self::$factory;

        $website = $factory::build($this->correct_url, 'our_website');

        self::assertInstanceOf('AppBundle\Model\OurWebsite', $website);
    }

    /**
     * Test creation of competitor
     */
    public function testFactoryCreationCompetitor()
    {
        $factory = self::$factory;

        $website = $factory::build($this->correct_url, 'competitor');

        self::assertInstanceOf('AppBundle\Model\Competitor', $website);
    }

    /**
     * Test creation of websites with unknown website type
     * @expectedException     InvalidArgumentException
     **/
    public function testFactoryCreationUnknownType()
    {
        $factory = self::$factory;

        $website = $factory::build($this->correct_url, 'random type');

    }

    /**
     * Test creation of websites with correct urls
     * @dataProvider correctDataProvider
     * @param string $url
     * @param string $website_type
     */
    public function testFactoryCreationCorrectUrls(string $url, string $website_type){

        $factory = self::$factory;
        $website = $factory::build($url, $website_type);

        self::assertInstanceOf('AppBundle\Model\WebsiteInterface', $website);
    }

    /** Test creation of websites with incorrect urls
     * @dataProvider incorrectDataProvider
     * @expectedException InvalidArgumentException
     * @param string $url
     * @param string $website_type
     */
    public function testFactoryCreationIncorrectUrls(string $url, string $website_type)
    {
        $factory = self::$factory;

        $website = $factory::build($url, $website_type);

    }


}
