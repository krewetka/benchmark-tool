<?php

declare(strict_types = 1);

namespace AppBundle\Model;


/**
 * Class AbstractWebsiteTest
 * @package AppBundle\Model
 */
class AbstractWebsiteTest extends \PHPUnit_Framework_TestCase
{

    /** Data where our website is more than twice slower then competitor
     * @return array
     */
    public function reallySlowWebsiteDataProvider()
    {
        return [
            [9,4],
            [11,5],
            [21,1]
        ];
    }

    /**
     * Data where our website is slower then competitor but not twice slower
     * @return array
     */
    public function slowWebsiteDataProvider()
    {
        return [
            [4,3],
            [7,5],
            [2,1]
        ];
    }

    /**
     * Data where our website is faster than competitor
     * @return array
     */
    public function fastWebsiteDataProvider()
    {
        return [
            [2,4],
            [4,5],
            [6,15]
        ];
    }

    /**
     * Test for website twice slower than competitor
     * @dataProvider reallySlowWebsiteDataProvider
     * @param int $our_time
     * @param int $competitor_time
     */
    public function testIsWebsiteReallySlow(int $our_time, int $competitor_time)
    {

        $website1 =  $this->getMockBuilder(OurWebsite::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBenchmarkTime'])
            ->getMock();
        $website1->method('getBenchmarkTime')->willReturn($our_time);

        $website2 =  $this->getMockBuilder(Competitor::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBenchmarkTime'])
            ->getMock();
        $website2->method('getBenchmarkTime')->willReturn($competitor_time);

        $muchSlowerResult = $website1->isWebsiteMuchSlowerThan($website2);
        self::assertTrue($muchSlowerResult);

        //if it's much slower it's slower as well

        $slowerResult = $website1->isWebsiteSlowerThan($website2);
        self::assertTrue($slowerResult);
    }

    /**
     * Test for website faster than competitors
     * @dataProvider fastWebsiteDataProvider
     * @param int $our_time
     * @param int $competitor_time
     */
    public function testIsWebsiteFast(int $our_time, int $competitor_time)
    {

        $website1 =  $this->getMockBuilder(OurWebsite::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBenchmarkTime'])
            ->getMock();
        $website1->method('getBenchmarkTime')->willReturn($our_time);

        $website2 =  $this->getMockBuilder(Competitor::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBenchmarkTime'])
            ->getMock();
        $website2->method('getBenchmarkTime')->willReturn($competitor_time);


        $slowerResult = $website1->isWebsiteSlowerThan($website2);
        self::assertFalse($slowerResult);

        //if it's not slower so for sure also not much slower

        $muchSlowerResult = $website1->isWebsiteMuchSlowerThan($website2);
        self::assertFalse($muchSlowerResult);
    }

    /**
     * Test for website slower than competitor but not twie slower
     * @dataProvider slowWebsiteDataProvider
     * @param int $our_time
     * @param int $competitor_time
     */
    public function testIsWebsiteSlowButNotReallySlow(int $our_time, int  $competitor_time)
    {
        $website1 =  $this->getMockBuilder(OurWebsite::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBenchmarkTime'])
            ->getMock();
        $website1->method('getBenchmarkTime')->willReturn($our_time);

        $website2 =  $this->getMockBuilder(Competitor::class)
            ->disableOriginalConstructor()
            ->setMethods(['getBenchmarkTime'])
            ->getMock();
        $website2->method('getBenchmarkTime')->willReturn($competitor_time);

        $slowerResult = $website1->isWebsiteSlowerThan($website2);
        self::assertTrue($slowerResult);

        //website is slow but not really slow so this should return false

        $muchSlowerResult = $website1->isWebsiteMuchSlowerThan($website2);
        self::assertFalse($muchSlowerResult);
    }



}
