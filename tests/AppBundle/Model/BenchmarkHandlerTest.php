<?php
declare(strict_types = 1);

namespace AppBundle\Model;

use AppBundle\Utils\HttpBenchmarkHandler;
use Monolog\Logger;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\Debug\TraceableEventDispatcher;

/**
 * Class BenchmarkHandlerTest
 * @package AppBundle\Model
 */
class BenchmarkHandlerTest extends KernelTestCase
{

    private $correctUrl = 'http://wp.pl';
    private $benchmarkTime = 10;

    private $benchmarkHandler;
    private static $websiteFactory;


    public static function setUpBeforeClass()
    {
        $kernel = static::createKernel();
        $kernel->boot();
        $container = $kernel->getContainer();

        self::$websiteFactory = $container->get('app.website.factory');

    }

    public function setUp()
    {

        $logger =  $this->createMock(Logger::class);

        $httpBenchmark =  $this->getMockBuilder(HttpBenchmarkHandler::class)
            ->setMethods(['getUrlLoadingTime'])
            ->disableOriginalConstructor()
            ->getMock();

        $httpBenchmark->method('getUrlLoadingTime')->willReturn($this->benchmarkTime);

        $dispatcher =  $this->createMock(TraceableEventDispatcher::class);

        $this->benchmarkHandler = new BenchmarkHandler($httpBenchmark, $dispatcher, $logger, self::$websiteFactory);

    }

    /**
     * Tests if performBenchmark function returns expected results
     */
    public function testPerformBenchmarkCorrect(){

        $ourWebsite = $this->correctUrl;
        $competitors =  [$this->correctUrl, $this->correctUrl];

        $allWebsitesCount = count($competitors) + count($ourWebsite);

        $benchmarkResult = $this->benchmarkHandler->performBenchmark($ourWebsite, $competitors);

        self::assertContainsOnlyInstancesOf(WebsiteInterface::class, $benchmarkResult);
        self::assertCount($allWebsitesCount, $benchmarkResult);

        foreach($benchmarkResult as $website){
            self::assertEquals($this->benchmarkTime, $website->getBenchmarkTime());
        }

    }


}
