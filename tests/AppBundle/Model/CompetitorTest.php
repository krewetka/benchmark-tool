<?php
declare(strict_types = 1);

namespace AppBundle\Model;

use InvalidArgumentException;

/**
 * Class CompetitorTest
 * @package AppBundle\Model
 */
class CompetitorTest extends \PHPUnit_Framework_TestCase
{

    private $correctUrl = 'http://wp.pl';
    private $incorrectUrl = 'kttp://aaa';

    /**
     * Tests creation of competitor with incorrect url
     * @expectedException     InvalidArgumentException
     **/
    public function testConstructorWithIncorrectParam(){

        $competitor = new Competitor($this->incorrectUrl);
    }

    /**
     * Tests creation of competitor with correct url
     */
    public function testConstructorWithCorrectParam(){

        $competitor = new Competitor($this->correctUrl);

        static::assertInstanceOf('AppBundle\Model\Competitor', $competitor);
        static::assertEquals('COMPETITOR', $competitor->getWebsiteType());
    }




}
