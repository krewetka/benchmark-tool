<?php
declare(strict_types = 1);

namespace AppBundle\Listeners;

use AppBundle\Events\BenchmarkEvent;
use AppBundle\Model\OurWebsite;
use AppBundle\Utils\NotificationHandler;
use Monolog\Logger;

class SlowWebsiteListenerTest extends \PHPUnit_Framework_TestCase
{

    private $event;
    private $logger;

    public function setUp()
    {

        $event =  $this->createMock(BenchmarkEvent::class);
        $website =  $this->createMock(OurWebsite::class);
        $event->method('getWebsite')->willReturn($website);

        $this->event = $event;

        $this->logger =  $this->createMock(Logger::class);

    }

    /**
     * Tests if notifiation by email is run for slow website
     */
    public function testOnBenchmarkSlow()
    {

        $notificationHandler =  $this->createMock(NotificationHandler::class);

        $notificationHandler->expects(self::once())
            ->method('notifyByEmail');

        $listener = new SlowWebsiteListener($notificationHandler,$this->logger);
        $listener->onBenchmarkSlow($this->event);

    }

    /**
     * Tests if notifiation by email and smsm is run for extra slow websites
     */
    public function testOnBenchmarkExtraSlow()
    {

        $notificationHandler =  $this->createMock(NotificationHandler::class);

        $notificationHandler->expects(self::once())
            ->method('notifyByEmail');

        $notificationHandler->expects(self::once())
            ->method('notifyBySMS');

        $listener = new SlowWebsiteListener($notificationHandler,$this->logger);
        $listener->onBenchmarkExtraSlow($this->event);

    }
}
