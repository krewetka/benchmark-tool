# Benchmark Console Application #
### Benchmark loading times between selected website and its competitors. ###
## Requirements ##
- PHP7
- Composer
- GIT (optional)

## Installation ##
1. Clone this repository `git clone https://bitbucket.org/krewetka/benchmark-tool.git`
2 Alternatively you can use download source code from https://bitbucket.org/krewetka/benchmark-tool/downloads
3. Go inside source directory and run `composer install`
4. Enter necessary params to generate *parameters.yml*

### Required: ###
* *app.notification_email_address* - email to notify about slow website results
* *app.notification_phone_number* - phone number to notify with sms about slow website
* *mailer params ( transport, host, user, password)* - to be able to send email
* *mailer_sender_email* - email address used in "from" field in email
* *symfony framework secret* - default can be left here for tests

## Usage Instructions ##

Application can be run in two ways, using:

* `app/console benchmark`
* `benchmark`

As parameters you enter:

1. *first parameter* - url address  of main website for benchmark
2. *next parameters* - url addresses of competitors

### Usage example ###

`benchmark http://forsal.pl http://wp.pl http://onet.pl` 

*Usually forsal.pl is much slower than others ;-)*

You can see parameters and other info using `benchmark --help`

## Result view ##

Websites are sorted by the benchmark time:

![benchmark.jpg](http://companion.pl/benchmark.jpg)

There is also `log.txt` created in main directory (for easy access).

## Notes ##

This app uses https://www.smsapi.pl/ notification service - real sms should be delivered when website is really slow.